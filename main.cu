#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <sys/time.h>
#include <time.h>
#include "ising.cuh"
#include "utils.cuh"

#define RANDOM_SEED 1235464

int main(int argc, char *argv[]) {
    int8_t *sim_results, *d_sim_results, *d_initial_state;
    int8_t *seq_results, *seq_initial_state;
    int model_size, chunk_size, augmented_model_size, max_iter;
    cudaDeviceProp prop;
    cudaError_t cudaError;
    cudaEvent_t start, stop;
    struct timeval seq_start, seq_end;
    float cuda_elapsed_time;
    long seq_elapsed_time;
    long unsigned int shared_memory_per_block;
    dim3 dimGrid, dimBlock;

    // Basic passed parameters checking
    if(argc != 4) {
        printf("Usage:\n        ./main <size_of_model> <chunk_size> <iterations>\n\n");
        return -1;
    }
    model_size = (atoi(argv[1]) >= 0) ? atoi(argv[1]) : 0;
    chunk_size = (atoi(argv[2]) >= 0) ? atoi(argv[2]) : 0;
    max_iter   = (atoi(argv[3]) >= 0) ? atoi(argv[3]) : 0;
    if(model_size == 0 || max_iter == 0 || chunk_size == 0) {
        printf("Model size/Chunk size/Maximum Iterations invalid\nEXITING...\n");
        return -2;
    }
    if(model_size < chunk_size) {
        printf("Model size = %d\nChunk size = %d\n", model_size, chunk_size);
        printf("Model size must be greather than chunk size\nEXITING...\n");
        return -3;
    }

    // Print GPU stats just for fun (or not?)
    print_gpu_stats();
    cudaGetDeviceProperties(&prop, 0);

    // Calculate Grid and Block structure in device
    calculate_grid_block_structure(model_size, chunk_size, prop, &dimGrid, &dimBlock);
    augmented_model_size = chunk_size * dimGrid.x * dimBlock.x;
    shared_memory_per_block = dimBlock.x * dimBlock.y * chunk_size * chunk_size * sizeof(int8_t);

    // Check if requested chunk size satisfies shared memory restrictions
    if(shared_memory_per_block >= prop.sharedMemPerBlock) {
        printf("Chunk size requested produces prohibitive amount of requested shared memory per block\nEXITING...\n");
        return -4;
    }

    // Allocate memory on host (sim_results) and on device (d_sim_results, d_initial_state) for CUDA implementation (augmented size arrays)
    sim_results       = (int8_t *)malloc(augmented_model_size * augmented_model_size * sizeof(int8_t));
    cudaMalloc((void**)&d_sim_results,   augmented_model_size * augmented_model_size * sizeof(int8_t));
    cudaMalloc((void**)&d_initial_state, augmented_model_size * augmented_model_size * sizeof(int8_t));

    // Allocate memory on host (seq_results, seq_initial_state) for sequential implementation
    seq_results       = (int8_t *)malloc(model_size * model_size * sizeof(int8_t));
    seq_initial_state = (int8_t *)malloc(model_size * model_size * sizeof(int8_t));

    // seq_initial_state will hold the original array for both implementations 
    initialize_array(seq_initial_state, model_size, RANDOM_SEED);

    // Use sim_results to temporary hold initial state for CUDA device and then transfer contents to d_initial_state
    for(int i = 0; i < model_size; i++)
        for(int j = 0; j < model_size; j++)
            sim_results[i*augmented_model_size + j] = seq_initial_state[i*model_size + j];
    cudaMemcpy(d_initial_state, sim_results, augmented_model_size * augmented_model_size * sizeof(int8_t), cudaMemcpyHostToDevice);

    // Call kernel and count time
    cudaEventCreate(&start);
    cudaEventRecord(start, 0);
    for(int iter = 0; iter < max_iter; iter++) {
        ising_simulation <<<dimGrid, dimBlock, shared_memory_per_block>>> (model_size, augmented_model_size, chunk_size, d_initial_state, d_sim_results);
        // cudaMemcpy(sim_results, d_sim_results, augmented_model_size * augmented_model_size * sizeof(int8_t), cudaMemcpyDeviceToHost);
        // print_state(sim_results, augmented_model_size);
        swap_arrays(&d_sim_results, &d_initial_state);
    }
    swap_arrays(&d_sim_results, &d_initial_state);
    cudaError = cudaGetLastError();
    cudaEventCreate(&stop);
    cudaEventRecord(stop, 0);
    cudaEventSynchronize(stop);
    cudaEventElapsedTime(&cuda_elapsed_time, start, stop);
    if(cudaError != 0) {
        printf("CUDA ERROR %d\nEXITING...\n", cudaError);
        return -5;
    }

    // Copy results from device memory to host memory
    cudaMemcpy(sim_results, d_sim_results, augmented_model_size * augmented_model_size * sizeof(int8_t), cudaMemcpyDeviceToHost);

    // Now run sequential implementation
    gettimeofday(&seq_start, NULL);
    for(int iter = 0; iter < max_iter; iter++) {
        host_ising_simulation_seq(model_size, seq_initial_state, seq_results);
        swap_arrays(&seq_initial_state, &seq_results);
    }
    swap_arrays(&seq_initial_state, &seq_results);
    gettimeofday(&seq_end, NULL);
    seq_elapsed_time = (seq_end.tv_sec - seq_start.tv_sec)*1000000 + (seq_end.tv_usec - seq_start.tv_usec);

    // Print elapsed time
    printf("|================== Execution times ==================|\n");
    printf("  CPU-Sequential elapsed time = %f us\n", (float)seq_elapsed_time);
    printf("  CUDA elapsed time           = %f us\n", 1000*cuda_elapsed_time);

    // Check outputs if are the same
    for(int i = 0; i < model_size; i++) {
        for(int j = 0; j < model_size; j++) {
            if(sim_results[i*augmented_model_size + j] != seq_results[i*model_size + j]) {
                printf("\nERROR: Outputs don't match in (%u,%u) element\nCUDA: %d   Host: %d\n", i, j, sim_results[i*augmented_model_size + j], seq_results[i*model_size + j]);
                return -6;
            }
        }
    }
    printf("\nOutputs checked and are the same\n");

    // Free memory
    free(sim_results);
    free(seq_results);
    free(seq_initial_state);
    cudaFree(d_initial_state);
    cudaFree(d_sim_results);

    return 1;
}