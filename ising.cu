#include <stdint.h>
#include <stdio.h>
#include "ising.cuh"
#include "utils.cuh"

__global__ void ising_simulation(int n, int augmented_n, int chunk_size, int8_t* old_state, int8_t* new_state) {
    extern __shared__ int8_t old_state_shared[];

    int i_initial = (blockIdx.x * blockDim.x + threadIdx.x) * chunk_size;
    int j_initial = (blockIdx.y * blockDim.y + threadIdx.y) * chunk_size;

    for(int i = 0; i < chunk_size; i++) {
        for(int j = 0; j < chunk_size; j++) {
            old_state_shared[(threadIdx.x*chunk_size + i) * (blockDim.y*chunk_size) + (threadIdx.y*chunk_size + j)] = 
                        old_state[(((i+i_initial) % n + n) % n)*augmented_n + (((j+j_initial) % n + n) % n)];
        }
    }
    __syncthreads();

    int8_t sum;
    for(int i = 0; i < chunk_size; i++) {
        for(int j = 0; j < chunk_size; j++) {
            if( ((threadIdx.x*chunk_size + i) % (blockDim.x*chunk_size - 1) == 0) || ((threadIdx.y*chunk_size + j) % (blockDim.y*chunk_size - 1) == 0) ) {
                sum = old_state[(((i+i_initial-1) % n + n) % n)*augmented_n + (j+j_initial)] +  // (i-1,j)
                      old_state[(i+i_initial)*augmented_n + (((j+j_initial-1) % n + n) % n)] +  // (i,j-1)
                      old_state[(i+i_initial)*augmented_n + (((j+j_initial+1) % n + n) % n)] +  // (i,j+1)
                      old_state[(((i+i_initial+1) % n + n) % n)*augmented_n + (j+j_initial)] +  // (i+1,j)
                      old_state_shared[(threadIdx.x*chunk_size + i) * (blockDim.y*chunk_size) + (threadIdx.y*chunk_size + j)];  // (i,j)
            } else {
                sum = old_state_shared[(threadIdx.x*chunk_size + i - 1) * (blockDim.y*chunk_size) + (threadIdx.y*chunk_size + j)] + // (i-1,j)
                      old_state_shared[(threadIdx.x*chunk_size + i) * (blockDim.y*chunk_size) + (threadIdx.y*chunk_size + j - 1)] + // (i,j-1)
                      old_state_shared[(threadIdx.x*chunk_size + i) * (blockDim.y*chunk_size) + (threadIdx.y*chunk_size + j + 1)] + // (i,j+1)
                      old_state_shared[(threadIdx.x*chunk_size + i + 1) * (blockDim.y*chunk_size) + (threadIdx.y*chunk_size + j)] + // (i+1,j)
                      old_state_shared[(threadIdx.x*chunk_size + i) * (blockDim.y*chunk_size) + (threadIdx.y*chunk_size + j)];      // (i,j)
            }
            new_state[(i+i_initial)*augmented_n + (j+j_initial)] = (!!sum) | (sum >> 7);     // sign(sum)
        }
    }
}

__host__ void host_ising_simulation_seq(int n, int8_t* old_state, int8_t* new_state) {
    int8_t sum;
    for(int i = 0; i < n; i++) {
        for(int j = 0; j < n; j++) {
            sum = old_state[(((i-1) % n + n) % n)*n + j] +  // (i-1,j)
                  old_state[i*n + (((j-1) % n + n) % n)] +  // (i,j-1)
                  old_state[i*n + (((j+1) % n + n) % n)] +  // (i,j+1)
                  old_state[(((i+1) % n + n) % n)*n + j] +  // (i+1,j)
                  old_state[i*n + j];                       // (i,j)
            new_state[i*n + j] = (!!sum) | (sum >> 7);      // sign(sum)
        }
    }
}