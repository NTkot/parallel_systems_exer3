#ifndef ISING_CUH_INCLUDED
#define ISING_CUH_INCLUDED

__global__ void ising_simulation(int n, int augmented_n, int chunk_size, int8_t* old_state, int8_t* new_state);
__host__ void host_ising_simulation_seq(int n, int8_t* old_state, int8_t* new_state);

#endif