#ifndef UTILS_CUH_INCLUDED
#define UTILS_CUH_INCLUDED

__host__ void print_gpu_stats();
__host__ void print_state(int8_t *state, int model_size);
__host__ void initialize_array(int8_t * initial_state, int size, unsigned int seed);
__host__ void swap_arrays(int8_t **array1, int8_t **array2);
__host__ void calculate_grid_block_structure(int model_size, int chunk_size, cudaDeviceProp properties, dim3* DimGrid, dim3* DimBlock);

#endif