COMPILER=nvcc
FLAGS=-O2

all: main

main: main.o ising.o utils.o
	${COMPILER} ${FLAGS} main.o ising.o utils.o -o main

main.o: main.cu
	${COMPILER} -c ${FLAGS} main.cu

utils.o: utils.cu
	${COMPILER} -c ${FLAGS} utils.cu -lm

ising.o: ising.cu
	${COMPILER} -c ${FLAGS} ising.cu

clean:
	rm *.o main
