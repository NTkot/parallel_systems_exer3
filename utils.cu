#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <math.h>
#include "utils.cuh"

__host__ void print_gpu_stats() {
    cudaDeviceProp prop;

    cudaGetDeviceProperties(&prop, 0);

    printf("|==================== Device Properties ====================|\n");
    printf("  \"%s\"\n", prop.name);
    printf("  CUDA Capability (Major.minor) version: %d.%d\n", prop.major, prop.minor);
    printf("  Total amount of Global memory:         %lu MB\n", prop.totalGlobalMem/1048576);
    printf("  Shared memory available per block:     %lu KB\n", prop.sharedMemPerBlock/1024);
    printf("  Streaming Multiprocessor(SM) Count:    %d\n", prop.multiProcessorCount);
    printf("  Maximum threads per block:             %d\n", prop.maxThreadsPerBlock);
}

__host__ void print_state(int8_t *state, int model_size) {
    for(int i = 0; i < model_size; i++) {
        for(int j = 0; j < model_size; j++) {
            printf("%d ", (state[i*model_size + j] + 1) / 2);
        }
        printf("\b\n");
    }
    printf("\n");
}

__host__ void initialize_array(int8_t * initial_state, int size, unsigned int seed) {
    // Seed for generating numbers using define macro in main.cu
    srand(seed);

    // Randomly choose -1 or 1 for each of the elements
    for(uint64_t i = 0; i < (uint64_t) size * (uint64_t) size; i++)
        initial_state[i] = 2*(rand() % 2) - 1;
}

__host__ void swap_arrays(int8_t **array1, int8_t **array2) {
    int8_t* temp = *array1;
    *array1 = *array2;
    *array2 = temp;
}

__host__ void calculate_grid_block_structure(int model_size, int chunk_size, cudaDeviceProp properties, dim3* dimGrid, dim3* dimBlock) {
    int maxThreadsPerBlock = properties.maxThreadsPerBlock;
    int maxArrayDimPerBlock = (int)sqrt(maxThreadsPerBlock);

    dimGrid->x  = model_size / (maxArrayDimPerBlock * chunk_size) + ((model_size % (maxArrayDimPerBlock * chunk_size) == 0) ? 0 : 1);
    dimGrid->y  = dimGrid->x;
    dimBlock->x = model_size / (dimGrid->x * chunk_size) + ((model_size % (dimGrid->x * chunk_size) == 0) ? 0 : 1);
    dimBlock->y = dimBlock->x;
    dimGrid->z  = 1;
    dimBlock->z = 1;

    printf("\n|============= Kernel function - Device configuration =============|\n");
    printf("  Grid size  (Blocks)       = %u x %u = %u blocks\n", dimGrid->x, dimGrid->y, dimGrid->x * dimGrid->y);
    printf("  Block size (Threads)      = %u x %u = %u threads/block\n", dimBlock->x, dimBlock->y, dimBlock->x * dimBlock->y);
    printf("  Augmented model dimension = (%u x %u) x %d = %u\n", dimGrid->x, dimBlock->x, chunk_size, chunk_size * dimGrid->x * dimBlock->x);
    printf("  Requested shared memory   = %lu Bytes\n\n", dimBlock->x * dimBlock->y * chunk_size * chunk_size * sizeof(int8_t));
}